<?php
/**
 */
 
require_once __DIR__ . '/Components/CSRFWhitelistAware.php'; 
 
 
class Shopware_Plugins_Frontend_CojuOnPage_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{     

	private $subshops = null;

    public function getCapabilities()
    {
        return array(
			'install' => true,
			'update' => true,
			'enable' => true	
		);
    }

    public function getLabel()
    {
        return 'Onpage Optimierung';
    }

    public function getVersion()
    {
        return '1.1.0';
    }

    /**
    * Gibt die gesammelten Plugin-Informationen zur�ck
    *
    */
    public function getInfo() {
        return array(
            // Die Plugin-Version.
            'version' => $this->getVersion(),
            // Copyright-Hinweis
            'copyright' => 'Copyright (c) 2016, Conversion Junkies 2.0 GmbH',
            // Lesbarer Name des Plugins
            'label' => $this->getLabel(),
			
			'author' => 'Conversion Junkies 2.0 GmbH ',
			
            // Info-Text, der in den Plugin-Details angezeigt wird
            'description' => file_get_contents($this->Path() . 'info.txt'),
            // Anlaufstelle f�r den Support
            'support' => 'http://www.conversion-junkies.de',
            // Hersteller-Seite
            'link' => 'http://www.conversion-junkies.de',
            // �nderungen
            'changes' => array(
                '1.0.0'=> array(
					'releasedate'=>'2016-12-10',
					'lines' => array(
						'1. Release'
					)
				)
			),
            // Aktuelle Revision des Plugins
            'revision' => '0'
        );
    }

    
    public function update($version)
    {
        return array(
			'success' => true, 
			'invalidateCache' => array(
				'backend',
				'frontend',
				'proxy'
			)
		);
    }
	
    public function install()
    {                                       
        $metaDataCache  = Shopware()->Models()->getConfiguration()->getMetadataCacheImpl();
        $metaDataCache->deleteAll();
        
        // SW 5.2 supported
      
            $service = $this->get('shopware_attribute.crud_service');
                   
			// Kategorien
				   
			$service->update(
				's_categories_attributes',
				'coju_onpage_extended',
				'html', 
				[
					'label' => 'Erweiterter Text', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 5, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);

			$service->update(
				's_categories_attributes',
				'coju_onpage_sidebar',
				'html', 
				[
					'label' => 'Text Sidebar', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 5, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);
			
			$service->update(
				's_categories_attributes',
				'coju_onpage_index',
				'boolean', 
				[
					'label' => 'Deindiziert', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 10, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);
			
			$service->update(
				's_categories_attributes',
				'coju_onpage_infooter',
				'boolean', 
				[
					'label' => 'In Footer Navigation', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 20, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);
			
			// hersteller
			
			
			
			$service->update(
				's_articles_supplier_attributes',
				'coju_onpage_city',
				'string', 
				[
					'label' => 'Stadt , Land', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 5, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);		
			
			$service->update(
				's_articles_supplier_attributes',
				'coju_onpage_postcode',
				'string', 
				[
					'label' => 'Postleitzahl', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 5, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);
			
			$service->update(
				's_articles_supplier_attributes',
				'coju_onpage_address',
				'string', 
				[
					'label' => 'Adresse', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 5, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);
			
			$service->update(
				's_articles_supplier_attributes',
				'coju_onpage_index',
				'boolean', 
				[
					'label' => 'Deindiziert', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 10, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);
			
			
			$service->update(
				's_articles_supplier_attributes',
				'coju_onpage_infooter',
				'boolean', 
				[
					'label' => 'In Footer Navigation', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 20, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);
			
			
			
			
			// Artikel
			
        	$service->update(
				's_articles_attributes',
				'coju_onpage_index',
				'boolean', 
				[
					'label' => 'Deindiziert', 
					'translatable' => true, 
					'displayInBackend' => true, 
					'position' => 5, 
					'custom' => true, 
					'pluginId' => $this->getId()
				]
			);
        	
        
			$metaDataCache  = Shopware()->Models()->getConfiguration()->getMetadataCacheImpl();
			$metaDataCache->deleteAll();

			Shopware()->Models()->generateAttributeModels(
				array('s_categories_attributes')
			);
        
			$this->subscribeEvent(
				'Enlight_Controller_Action_PostDispatch_Backend_Category',
				'postDispatchCategory'
			);

			$this->subscribeEvent(
				'Enlight_Controller_Action_PostDispatch', 
				'onPostDispatchFrontend'
			);		
  
			$this->subscribeEvent(
				'Theme_Compiler_Collect_Plugin_Less',
				'addLessFiles'
			);        
			
			$this->subscribeEvent(
				'Enlight_Controller_Dispatcher_ControllerPath_Backend_OnPage',
				'onGetBackendController'
			);
			
			$this->subscribeEvent(
				'Enlight_Controller_Action_PostDispatchSecure_Frontend_Listing',
				'onPostDispatchListing',
				100
			);
			
			$this->subscribeEvent(
				'Enlight_Controller_Action_PostDispatch_Frontend_Detail',
				'onPostDispatchDetail',
				150
			);
			
			
			$this->createMenu();
			
			return array(
				'success' => true,
				'invalidateCache' => array(
					'backend', 
					'frontend', 
					'proxy'
				)
			);
    }

    /**
     * Plugin uninstall method
     *
     * @return bool success
     */
    public function uninstall()
    {			
    	
		
        Shopware()->Models()->removeAttribute('s_categories_attributes', 'coju', 'onpage_extended');
        Shopware()->Models()->removeAttribute('s_categories_attributes', 'coju', 'onpage_sidebar');
		Shopware()->Models()->removeAttribute('s_categories_attributes', 'coju', 'onpage_index');
		Shopware()->Models()->removeAttribute('s_articles_supplier_attributes', 'coju', 'onpage_index');
		Shopware()->Models()->removeAttribute('s_articles_attributes', 'coju', 'onpage_index');
        
        $metaDataCache  = Shopware()->Models()->getConfiguration()->getMetadataCacheImpl();
        $metaDataCache->deleteAll();

        Shopware()->Models()->generateAttributeModels(
            array('s_categories_attributes')
        );
        
        return array(
			'success' => true, 
			'invalidateCache' => array(
				'backend',
				'frontend',
				'proxy'
			)
		);
    }

    /**
     * Creates the Favorites backend menu item.
     *
     * The Favorites menu item opens the listing for the SwagFavorites plugin.
     */
    public function createMenu()
    {
        $this->createMenuItem(
            array(
                'label' => 'OnPage',
                'onclick' => 'Shopware.ModuleManager.createSimplifiedModule("OnPage", { "title": "OnPage" })',
                'class' => 'sprite-application-icon-large',
                'active' => 1,
                'parent' => $this->Menu()->findOneBy(['label' => 'Einstellungen'])
            )
        );
    }
	
	 /**
     * Returns the path to the controller.
     *
     * Event listener function of the Enlight_Controller_Dispatcher_ControllerPath_Backend_SwagFavorites
     * event.
     * Fired if an request will be root to the own Favorites backend controller.
     *
     * @return string
     */
    public function onGetBackendController()
    {
		// SUBSHOP
		
		$sql = 'SELECT id , name FROM `s_core_shops`';
		
		$subshopResult = Shopware()->Db()->fetchAll($sql);
		
		$this->subshops = array();
		
		foreach( $subshopResult as $shop){
		
			$this->subshops[$shop['id']] = $shop;
			
		}
		
		$this->get('template')->assign('subshops', $this->subshops);
		
        $this->get('template')->addTemplateDir($this->Path() . 'Views/');

        return $this->Path() . 'Controllers/OnPage.php';
    }
	
    /**
     * Provide the file collection for less
     *
     * @param Enlight_Event_EventArgs $args
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function addLessFiles(Enlight_Event_EventArgs $args)
    {
        
		/*
		$less = new \Shopware\Components\Theme\LessDefinition(
        //configuration
            array(),
            //less files to compile
            array(
                __DIR__ . '/Views/frontend/_public/src/less/all.less'
            ),
            //import directory
            __DIR__
        );
        return new Doctrine\Common\Collections\ArrayCollection(array($less));
		*/
    }  
    
    /**
     * @param Enlight_Event_EventArgs $args
     * @return string
     */
    public function postDispatchCategory(Enlight_Event_EventArgs $args)
    {
		
		/*
        $args->getSubject()->View()->addTemplateDir(
            $this->Path() . 'Views/'
        );

        // SW 5.2 supported - Not load > 5.2
        if (!$this->assertMinimumVersion('5.2')) { 
            if ($args->getRequest()->getActionName() === 'load') {
                $args->getSubject()->View()->extendsTemplate(
                    'backend/category/model/six_seotext/attribute.js'
                );
                $args->getSubject()->View()->extendsTemplate(
                    'backend/category/view/category/six_seotext/window.js'
                );
            }
        }
		*/
    }    

  	public function onPostDispatchFrontend(Enlight_Event_EventArgs $args) 
    {    
	
        $request  = $args->getSubject()->Request();
		$response = $args->getSubject()->Response();
		$view     = $args->getSubject()->View();
		
		$requestCategoryId = $request->getParam('sCategory');
		
		
		
		
		
		if($request->getModuleName() == "widgets" && !empty($requestCategoryId)){
			
			$categoryContent = Shopware()->Modules()->Categories()->sGetCategoryContent($requestCategoryId);
			$view->assign([
				'sCategoryContent' => $categoryContent
			]);
					
		}
		
		if (!$request->isDispatched() || $response->isException() || $request->getModuleName() != "frontend" || !$view->hasTemplate()) {
			return null;
		}
	
		
		// LOAD Hersteller-Seite
		
		$subshop = Shopware()->Shop()->getId();
		
		//@tddo Subshop
		$sql = 'SELECT SAS.id ,SAS.name FROM `s_articles_supplier_attributes` SASA
				INNER JOIN `s_articles` SA ON SA.supplierID = SASA.supplierID
				INNER JOIN `s_articles_supplier` `SAS` ON SAS.id = SASA.supplierID
				WHERE `coju_onpage_infooter` = 1
				AND SA.active = 1
				GROUP BY SAS.id
		';
		
		$suppliers = Shopware()->Db()->fetchALL(
			$sql
		);
		
		// KATEGORIEN
		
		
		//@tddo Subshop
		$sql = 'SELECT SC.id , SC.description FROM `s_categories_attributes` SCA
				INNER JOIN s_categories SC ON SC.id = SCA.categoryID
				WHERE `coju_onpage_infooter` = 1
				AND SC.active = 1
				ORDER BY SC.description
				';
		
		$categories = Shopware()->Db()->fetchALL(
			$sql
		);
		
		// KATEGORIEN
			
		$baseUrl = Shopware()->Config()->get('baseFile') . '?sViewport=cat&sCategory=';	
		
		foreach($categories as &$category){
		
			//$blogBaseUrl = Shopware()->Config()->get('baseFile') . '?sViewport=blog&sCategory='; 
		
			$category['link'] = $baseUrl . $category['id'];
		
			//$url = $category['category']['blog'] ? $blogBaseUrl : $baseUrl;
			//$url = $category['category']['blog'] ? $blogBaseUrl : $baseUrl;
			
						
		}

		$view->assign([
			'sFooterSuppliers' => $suppliers,
			'sFooterCategories' => $categories
		]);
		
		$view->assign([
			'sFooterSuppliers' => $suppliers
		]);

		$view->addTemplateDir($this->Path() . "Views/");
		
		
			
  	}
	
	
	public function onPostDispatchListing(Enlight_Event_EventArgs $arguments)
    {
		
		$deIndex = false;
		
		$controller = $arguments->getSubject();
        $view = $controller->View();
		
		$sCategoryContent = $view->getAssign('sCategoryContent');
		
		if(empty($sCategoryContent['cmsheadline'])){
			
			$sCategoryContent['cmsheadline'] = $sCategoryContent['description'];
			
		}
		
		$view->assign('sCategoryContent',$sCategoryContent);
		
		$view->addTemplateDir($this->Path() . 'Views/');	
		
		
		if(isset($sCategoryContent['attribute']['coju_onpage_index']) AND !empty($sCategoryContent['attribute']['coju_onpage_index'])){
			
			$deIndex = true;
		}
		
		$manufacturer = $view->getAssign('manufacturer');
	
		if(!empty($manufacturer)){
			
			/*
			if($manufacturer->getAttributes()['core']->get('coju_onpage_index')){
				
				$deIndex = true;
				
			}
			
			
			$schema = [];
			$schema['name'] = $manufacturer->getName();
			$schema['city'] = $manufacturer->getAttributes()['core']->get('coju_onpage_city');
			$schema['postcode'] = $manufacturer->getAttributes()['core']->get('coju_onpage_postcode');
			$schema['address'] = $manufacturer->getAttributes()['core']->get('coju_onpage_address');
			
			$view->assign('coju_onpage_schema',$schema);
			*/
		}
		
		$view->assign('coju_onpage_deindex',$deIndex);
			
	}
	
	public function onPostDispatchDetail(Enlight_Event_EventArgs $arguments)
    {
		
		$deIndex = false;
		
		$controller = $arguments->getSubject();
        $view = $controller->View();
		$view->addTemplateDir($this->Path() . 'Views/');	
		
		$sArticle = $view->getAssign('sArticle');
		
		
		if(!empty($sArticle)){
		
			/*
			if($sArticle['attributes']['core']->get('coju_onpage_index')){
				
				$deIndex = true;
				
			}
			*/
		
		}
		
		
		$view->assign('coju_onpage_deindex',$deIndex);
	}	
	
	
}