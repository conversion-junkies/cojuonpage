<?php

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr\Join;
use Shopware\Components\CSRFWhitelistAware;

/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Shopware_Controllers_Backend_OnPage extends Enlight_Controller_Action implements CSRFWhitelistAware
{
	
	
	public function getWhitelistedCSRFActions()
    {
        return [
			'index',
			'linkDetail',
			'exclude',
			'config',
			'getExportData',
			'setImportData',
			'getkeywords',
			'getLinks',			
			'deleteStopword',
			'deleteLinks',
			'deleteHook',
			'loadMetrics',
			'addStopword',
			'addHook',
			'saveLink',
			'getExclude',
			'getDefaultKeywords',
			
			
			
			'indexManagement',
			
		];
    }
	
	private $typeNames = array(
		'1' => array( 
			'id' => 'Detail',
			'name' => 'Detailseite'
		),
		'2' => array(
			'id' => 'Listing',
			'name' => 'Kategorieseite'
		),
		'3' => array(
			'id' => 'Index',
			'name' => 'Startseite'
		)
	);
	
	
    public function indexAction()
    {
		// get the active Subshop ID
		
		$subshop  = (int)$this->Request()->getParam('subshop', null);
		
		if(empty($subshop)){
			$sql = 'SELECT id , name FROM `s_core_shops` LIMIT 1';
		
			$result = Shopware()->Db()->fetchRow($sql);
			$subshop = $result['id'];
		
		}
		// load subshop
		
		$this->View()->assign(['subshop' => $subshop]);
    }
	
	public function indexManagementAction()
    {
		// get the active Subshop ID
		
		$subshop  = (int)$this->Request()->getParam('subshop', null);
		
		if(empty($subshop)){
			$sql = 'SELECT id , name FROM `s_core_shops` LIMIT 1';
		
			$result = Shopware()->Db()->fetchRow($sql);
			$subshop = $result['id'];
		
		}
		// load subshop
		
		$this->View()->assign(['subshop' => $subshop]);
    }

	public function excludeAction()
    {
    }
	
	public function configAction()
    {

		$sql = 'SELECT * FROM `coju_internallinks_options`';
		$hooks = Shopware()->Db()->fetchAll($sql);
		
		foreach($hooks as &$hook){
			
			$hook['name'] = $this->typeNames[ $hook['type'] ]['name'];
			
		}
	
		$this->View()->assign([ 
			'hooks' => $hooks , 
			'typeNames' => $this->typeNames
		]);
		
    }

	// 3 Zustände  Edit , new , newbyterm
	
	public function getExportDataAction(){
		
		$this->Front()->Plugins()->Json()->setRenderer();
		
		$sql = 'SELECT * FROM `s_link`';
		$links = Shopware()->Db()->fetchAll($sql);
		
				
		$sql = 'SELECT * FROM `s_stopwords`';
		$stopwords = Shopware()->Db()->fetchAll($sql);
		
		$this->View()->assign([ 'stopwords' => $stopwords ]);
		$this->View()->assign([ 'links' => $links ]);
		
		//@todo VERSION FOR IMPORT
		
	}
	
	
	public function addHookAction(){
		
		$this->Front()->Plugins()->Json()->setRenderer();
		
		$type = (int)$this->Request()->getParam('type', null);	
		$count = (int)$this->Request()->getParam('count', null);	
		$value = $this->Request()->getParam('field', null);	
		$subshop = (int)$this->Request()->getParam('subshop', null);	
		
		$values = array(
			'type' => $type,
			'value' => $value,
			'count' => $count,
			'subshop' => $subshop
		);
		
		$linkOption = new \Shopware\CustomModels\Linkoption\Linkoption;
		$linkOption->fromArray($values);
				
		Shopware()->Models()->persist($linkOption);
		Shopware()->Models()->flush();
	
		$values['id'] = $linkOption->getId();
				
		$this->View()->assign([ 'success' => true , 'data' => $values ]);
		
	}


	
	
	
	public function setImportDataAction(){
		
		$this->Front()->Plugins()->Json()->setRenderer();

		$code = json_decode( substr( file_get_contents($_FILES['file']['tmp_name']) , 1, -1 ) , true ) ;
		
		$sql = "
			TRUNCATE TABLE s_link;
			TRUNCATE TABLE s_stopwords;
		";
		Shopware()->Db()->query($sql);
		
		foreach( $code['stopwords'] as $item){
			
			$sql = "INSERT INTO `s_stopwords` (`id`, `term`,`custom` , `language`)
			VALUES (
			NULL,
			'" . $item['term'] . "',
			'" . $item['custom'] . "',
			'de_DE')";
			Shopware()->Db()->query($sql);
			
		}
		
		foreach( $code['links'] as $item){
			
			$link = new \Shopware\CustomModels\Link\Link;
			$link->fromArray($item);
					
			Shopware()->Models()->persist($link);
			Shopware()->Models()->flush();
			
		}
		
	}
	
	
	private function getEmptyArrayOfTable($name){
		
		$sql = 'SELECT COLUMN_NAME from information_schema.COLUMNS where TABLE_NAME = ?';
		$fields = Shopware()->Db()->fetchALL($sql , array($name));
		
		$array = array();
		
		foreach($fields as $field) {
			
			$array[$field['COLUMN_NAME']] = "";
			
		}
		
		return $array;
		
	}
	
	public function linkDetailAction(){
	
		$id = (int)$this->Request()->getParam('id', null);	
		$term = $this->Request()->getParam('term', null);	
		$subshop = (int)$this->Request()->getParam('subshop', null);	
		
		if(!empty($id)){
		
			// Edit 		
			
			$sql = 'SELECT * FROM `s_link` WHERE id = ?';
			$link = Shopware()->Db()->fetchRow($sql , array($id));
			$eventlog = "edit";
			
		} else {
	
			if(!empty($term)){

				// TERM			
			
				$link = $this->getEmptyArrayOfTable('s_link');
				$link['term'] = $term;
				$link['subshop'] = $subshop;
				$eventlog = "keyword";
				
			} else {
				
				// NEW
				
				$link = $this->getEmptyArrayOfTable('s_link');
				$link['subshop'] = $subshop;
				$eventlog = "new";
				
			}	
			
			// Fill by default;
			$link['follow'] = 1;
			$link['homepage'] = 1;
			$link['detail'] = 1;
			$link['emotion'] = 1;
			$link['category'] = 1;
			$link['article'] = 1;
			$link['blog'] = 1;
			$link['relevance'] = 50;

		}
		
		$this->View()->assign(['link' => $link]);
		$this->View()->assign(['eventlog' => $eventlog]);
		 
	}
	 
	
	public function deleteStopwordAction(){
		
		
		$this->Front()->Plugins()->Json()->setRenderer();
		
		$id = (int)$this->Request()->getParam('id', null);
        
		$sql = "DELETE FROM s_stopwords WHERE id = ?";
		
		Shopware()->Db()->query($sql, array($id));
		
		$this->View()->assign(['id' => $id]);
		
	}
	
	public function deleteLinksAction(){
		
		
		$this->Front()->Plugins()->Json()->setRenderer();
		
		$ids = (array)$this->Request()->getParam('ids', null);
		
		if(!empty($ids)){
        
			$sql = 'DELETE FROM s_link WHERE id IN (' . implode( ',' , $ids ) . ')';
			Shopware()->Db()->query($sql);
		
		}
		
		$this->View()->assign(['status' => 'success']);
		
	}
	
	public function deleteHookAction(){
		
		
		$this->Front()->Plugins()->Json()->setRenderer();
		
		$id = (int)$this->Request()->getParam('id', null);
        
		$sql = "DELETE FROM coju_internallinks_options WHERE id = ?";
		
		Shopware()->Db()->query($sql, array($id));
		
		$this->View()->assign([ 'status' => 'success']);
		
	}
	
	
	public function getExcludeAction()
    {
        $this->Front()->Plugins()->Json()->setRenderer();

		$sql = 'SELECT * FROM `s_stopwords`';
		
		$result = Shopware()->Db()->fetchAll($sql);

        $this->View()->assign(['data' => $result]);
    }
	
	
    public function getLinksAction()
    {
        $this->Front()->Plugins()->Json()->setRenderer();

		$subshop = (int)$this->Request()->getParam('subshop', null);
		
		// Fix for Shopware 5.4.+
		
		if(Shopware::VERSION >= 5.4){
			
			
			$sql = 'SELECT host , secure FROM `s_core_shops` WHERE id = ? ';
			$shop = Shopware()->Db()->fetchRow($sql , $subshop);
			
			$url = "http://" . $shop['host'];
			
			if($shop['secure']){

				$url = "https://" . $shop['host'];
		
			}
		
		}else{
			
			$sql = 'SELECT host , secure , always_secure FROM `s_core_shops` WHERE id = ? ';
			$shop = Shopware()->Db()->fetchRow($sql , $subshop);
			
			$url = "http://" . $shop['host'];
			
			if($shop['always_secure']){

				$url = "https://" . $shop['host'];
		
			}
			
		}
		
		$sql = 'SELECT * FROM `s_link` WHERE subshop = ? ';
			
		$result = Shopware()->Db()->fetchAll($sql , $subshop);
			
		foreach( $result as &$item ){
			
			// remove the Domain from internal Links	
			$item['link'] = str_replace($url , '' , $item['link']);			
			
			
		}
			
			

			 
			
        $this->View()->assign([
			'data' => $result
		]);
    }
	
    public function getKeywordsAction()
    {
        $this->Front()->Plugins()->Json()->setRenderer();

		$subshop = $this->Request()->getParam('subshop');
				
		$sql = 'SELECT C.id FROM `s_core_shops` as CS 
		INNER JOIN `s_categories` as C ON C.id = CS.category_id 
		WHERE CS.id = ?';
		$category = Shopware()->Db()->fetchOne($sql , array($subshop));

		$sql = 'SELECT CL.locale FROM `s_core_shops` as CS 
		INNER JOIN `s_core_locales` as CL ON CL.id = CS.locale_id 
		WHERE CS.id = ?';
		$locale = Shopware()->Db()->fetchOne($sql , array($subshop));

		
		// search for all Content in Shop by Category
		
		//@todo change to Param in prepare Statement
		
		$sql = '(SELECT C.cmstext as content FROM `s_categories` C WHERE (C.path LIKE "%|' . $category . '|" OR C.id = "'.$category.'"  ) AND C.cmstext != "")
		UNION
		(SELECT A.description_long as content FROM `s_categories` C INNER JOIN `s_articles_categories` AC ON AC.categoryID = C.id INNER JOIN `s_articles` A ON A.id = AC.articleID WHERE C.path LIKE "%|' . $category . '|")
		UNION
		(SELECT B.description as content FROM `s_categories` C INNER JOIN s_blog B ON B.category_id = C.id WHERE C.path LIKE "%|' . $category . '|");';
		
		$result = Shopware()->Db()->fetchAll($sql);
		
		$content = "";
		
		foreach($result as $item){
			
			$content .= $item['content'] . " \n";
		}
		
		$replaceLetters = array('&' , ')','(','"','\'','-','`','--',']','.','!',',',':','?','„','“');
		
		$content = html_entity_decode($content, ENT_QUOTES ,'UTF-8');
		
		//@todo - faster RegEx
		$content = preg_replace('|<\s?h[1-6].*>.*<\s?/\s?h[1-6]\s?>|U', "" , $content );
		$content = preg_replace('/<img.*>/U', "" , $content );
		$content = preg_replace('/<a.*>.*<\/a>/U', "" , $content );
		$content = str_replace(array("\n","\r","\t"), " " , $content);
		$content = str_replace($replaceLetters, " " , $content);
		$content = strip_tags($content);
		
		// load internal Stopwords 

		$builder = Shopware()->Models()->createQueryBuilder();
		$builder->select(array('stopword'))
			->from('Shopware\CustomModels\Stopword\Stopword', 'stopword')
			->where('stopword.language = :language')
			->setParameter('language' , $locale);
				
		$stopWordsData = $builder->getQuery()->getArrayResult();
		
		$stopWordsArray = array();
		
		foreach($stopWordsData as $item ) {
			
			$stopWordsArray[] = $item['term'];
		}
		
		//remove stopwords from Keywords
		$terms = array_count_values( array_filter( array_udiff( explode(' ', $content ) , $stopWordsArray , 'strcasecmp' ) ) );
		
		// Remove Linked Terms
		
		$builder = Shopware()->Models()->createQueryBuilder();
		$builder->select(array('link'))
			->from('Shopware\CustomModels\Link\Link', 'link')
			->where('link.subshop = :subshop')
			->setParameter('subshop' , $subshop);
		$exitTerms = $builder->getQuery()->getArrayResult();
		
		foreach($exitTerms as $item ) {
			
			unset($terms[$item['term']]);
		}

		$sort = $this->Request()->getParam('sort');
		switch($sort[0]['property']){
			
			case "term":
				
				if($sort[0]['direction'] == "ASC"){
					ksort($terms);
				} else {
					krsort($terms);
				}
				
			break;
			
			case "count":
				
				if($sort[0]['direction'] == "ASC"){
					asort($terms);
				} else {
					arsort($terms);
				}
			
			break;
			
		}	
					
		$returnArray = array();
		foreach($terms as $term => $count){
		
			$returnArray[] = array('term' => $term , 'count' => $count );
		
		}
		
		// @max Keywords
		$returnArray = array_slice( $returnArray , 0 , 1000);
	
		$this->View()->assign(['data' => $returnArray]);

    }
	
	public function loadMetricsAction(){
		
		$this->Front()->Plugins()->Json()->setRenderer();
		
		$term = $this->Request()->getParam('term', null);	
		
		$data = json_decode(file_get_contents('http://keyword.wpexplore.de/controller.php?keyword=' . urlencode( $term ) ));
		
		if($data->status == 'success' && $data->keyword != "null" ){
			
			$data = $data->data;
			
		}else {
			$data = "";
		}
		
		$this->View()->assign(['data' => $data]);
		
	}	
	
	
	public function addStopwordAction(){
		
		$this->Front()->Plugins()->Json()->setRenderer();
		
		
		$data = array(
			'custom' => 1
		
		);
		
		$stopword = new \Shopware\CustomModels\Stopword\Stopword;
		$params = $this->Request()->getParams();
		
		$data['term'] = $this->Request()->getParam('term');
		$data['language'] = $this->Request()->getParam('language');
				
		$stopword->fromArray($data);
		
		Shopware()->Models()->persist($stopword);
        Shopware()->Models()->flush(); 
		
		$data['id'] = $stopword->getId();	

		$this->View()->assign(['success' => $data]);
		
	}	
	
	
	public function saveLinkAction(){
		
		$this->Front()->Plugins()->Json()->setRenderer();
		
		$defaultValues = array(
			'title' => "",
			'cssclass' => "",
			'target' => 0,
			'follow' => 0,
			'homepage' => 0,
			'detail' => 0,
			'emotion' => 0,
			'category' => 0,
			'article' => 0,
			'blog' => 0,
		
		);
		
		$params = $this->Request()->getParams();
		$data = array_merge( $defaultValues , $params['data'] );

		if( !empty( $data['id'] )) {
		
			$link = Shopware()->Models()->find('\Shopware\CustomModels\Link\Link', $data['id'] );

		}else {
			
			$link = new \Shopware\CustomModels\Link\Link;

		}

		$link->fromArray($data);
				
		Shopware()->Models()->persist($link);
        Shopware()->Models()->flush();
		
		$data['id'] = $link->getId();	
			
		
		
		$this->View()->assign(['success' => $data]);
	}

	protected function getDefaultKeywordsAction() {
		
		// Load Default Stopwords from stopwords.info
		$this->Front()->Plugins()->Json()->setRenderer();
		
		//@todo andere URL
		$stopWordsData = json_decode(file_get_contents('http://internet-marketing-dresden.de/linkmole/default.php'));

		/*
		foreach($stopWordsData as $item){
		
			$sql = "INSERT INTO `s_stopwords` (`id`, `term`,`custom` , `language`)
			VALUES (
			NULL,
			'" . $item->name . "',
			'" . $item->custom . "',
			'de_DE')";
			Shopware()->Db()->query($sql);
		
		}*/
		
		return array(
            'success' => true,
            'total'   => count($stopWordsData),
            'data'    => $stopWordsData
        );
	}
	
	
    public function createSubWindowAction()
    {
		
		
    }
}
