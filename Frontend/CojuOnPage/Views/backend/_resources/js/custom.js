


var App = function() {
	
    jQuery(document).on('ready', _handleDocumentReady);
    jQuery(window).on('load', _handleWindowLoad);
    jQuery(window).on('resize', _handleWindowResize);

    function _handleDocumentReady() {

        init();
        panelSlide();
        adaptiveTableWidth();
        adaptiveTableHeight();
		
		//internalLinksTour();
		
    }

    function _handleWindowLoad() {

    }

    function _handleWindowResize() {
        adaptiveTableWidth();
        adaptiveTableHeight();
    }

    // Init

    function init() {
        jQuery('#links_paginate, #links_length').wrapAll('<div class="bottom-toolbar-left"></div>');
        jQuery('#keywords_paginate, #keywords_length').wrapAll('<div class="bottom-toolbar-right"></div>');
        jQuery('#exkeywords_paginate, #exkeywords_length').wrapAll('<div class="bottom-toolbar"></div>');
    }

    // Panel slider and adaptive table width

    var panelLeft = jQuery('.panel-display.left');
    var panelRight = jQuery('.panel-display.right');
    var tableLeft = jQuery('.table-left');
    var tableRight = jQuery('.table-right');

    function panelSlide() {
        jQuery('.panel-trigger').click(function() {
            jQuery(this).parent().hasClass('right') ? panelSlideLeft() : panelSlideRight();

        });
    }

    function panelSlideLeft() {

        panelRight.fadeOut();

        tableRight.css('z-index', '20').animate({
            width: '100%'
        }, {
            duration: '200',
            complete: function() {
                panelLeft.fadeIn();
                tableLeft.css('width', '0').css('z-index', '1');
                tableRight.css('z-index', '10').css('background', '#ebedef');
            }
        });
    }

    function panelSlideRight() {

        panelLeft.fadeOut();

        tableLeft.css('z-index', '20').animate({
            width: '100%'
        }, {
            duration: '200',
            complete: function() {
                panelRight.fadeIn();
                tableRight.css('width', '0').css('z-index', '1');
                tableLeft.css('z-index', '10').css('background', '#ebedef');
            }
        });
    }

    // Add window width to table if view is small and clear inline styles if view is large

    function adaptiveTableWidth() {

        jQuery(window).width() < 1100 ? jQuery('body').addClass('table-slide') : jQuery('body').removeClass('table-slide');

        setTimeout(function() {
            if (jQuery('body').hasClass('table-slide')) {
                jQuery('#links, #keywords, #list_hook').css('width', jQuery(window).width());

            } else {
                jQuery('.bottom-toolbar-left, .bottom-toolbar-right, .table-left, .table-right, .panel-display').removeAttr('style');
                jQuery('#links, #keywords, #list_hook').css('width', '100%');
            }
        }, 200);
    }

    //Add height to table to make them scrollable in large view

    function adaptiveTableHeight() {
        var windowHeight = jQuery(window).innerHeight();
        setTimeout(function() {
            jQuery('.wrapper').css('height', windowHeight);

        }, 200);
    }

    //Tour

    function internalLinksTour() {
        var tour = new Tour({
            steps: [{
                element: ".top-button.addlink",
                title: "Title of my step",
                content: "Links hinzufügen"
            }, {
                element: ".top-button.removelink",
                title: "Title of my step",
                content: "Links entfernen"
            }],
            template: '<div class="popover tour"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div><div class="popover-navigation"><button class="btn btn-default" data-role="prev">« zurück</button><span data-role="separator">|</span><button class="btn btn-default" data-role="next">vor »</button></div><button class="btn btn-default" data-role="end">Tour beenden</button></div>'
        });

        tour.init();

        tour.start();
    }
}

var App = new App();

