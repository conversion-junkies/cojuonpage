<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{link file="backend/_resources/css/bootstrap.min.css"}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
	
	<link rel="stylesheet" type="text/css" href="{link file="backend/_resources/css/datatables.min.css"}"/>
	<link rel="stylesheet" type="text/css" href="{link file="backend/_resources/css/custom.css"}"/>
	
	<!--TOUR -->
	<link rel="stylesheet" type="text/css" href="{link file="backend/_resources/css/bootstrap-tour.min.css"}"/>
	
</head>
<body role="document">

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
           
        </div>
        <div id="navbar" class="navbar-collapse">
		
            <ul class="nav navbar-nav">
								
				{foreach $subshops as $shop}
					
					 <li{if {controllerAction} === 'index' &&  {$shop.id} === {$subshop}} class="active"{/if}><a href="{url controller="OnPage" action="index" subshop="{$shop.id}"}">{$shop.name}</a></li>
	
				{/foreach}
			
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>




<div class="container theme-showcase" role="main">
    {block name="content/main"}{/block}
</div> <!-- /container -->

<script type="text/javascript" src="{link file="backend/base/frame/postmessage-api.js"}"></script>
<script type="text/javascript" src="{link file="backend/_resources/js/jquery-2.1.4.min.js"}"></script>
<script type="text/javascript" src="{link file="backend/_resources/js/bootstrap.min.js"}"></script>

<script type="text/javascript" src="{link file="backend/_resources/js/datatables.min.js"}"></script>
<script type="text/javascript" src="{link file="backend/_resources/js/custom.js"}"></script>

<script type="text/javascript" src="{link file="backend/_resources/js/bootstrap-tour.min.js"}"></script>



{block name="content/layout/javascript"}
<script type="text/javascript">
 

 
</script>
{/block}
{block name="content/javascript"}{/block}
</body>
</html>