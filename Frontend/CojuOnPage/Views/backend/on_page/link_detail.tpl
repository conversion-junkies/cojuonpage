{extends file="parent:backend/_base/detail.tpl"}

{block name="content/main"}
	
<form id="form-link-detail">

    <div class="panel-footer clearfix">
        <fieldset class="link-info">
			<legend>Linkinformationen</legend>
            <input type="hidden" id="id" name="id" value="{$link.id}" />

            <div class="col1">
                <label for="term">Term:</label>
                <input type="text" id="term" name="term" value="{$link.term}" placeholder="Term einfügen" required />
            </div>
            <div class="col2">
                <label for="term">Link:</label>
                <input type="text" name="link" value="{$link.link}" placeholder="Link einfügen" required />
            </div>
            <div class="col1">
                <label for="term">Title:</label>
                <input type="text" name="title" value="{$link.title}" placeholder="Title einfügen" />
            </div>
            <div class="col2">
                <label for="term">CSS:</label>
                <input type="text" name="cssclass" value="{$link.cssclass}" placeholder="CSS Klasse eingeben" />
            </div>
            <div class="col1 checkboxes">
                <label for="term">Follow:</label>
                <input type="checkbox" name="follow" value="1" {if {$link.follow}=="1" }checked{/if}>
            </div>
            <div class="col2 checkboxes">
                <label for="term">Target:</label>
                <input type="checkbox" name="target" value="1" {if {$link.target}=="1" }checked{/if}>
            </div>
            <input type="hidden" name="subshop" value="{$link.subshop}" />
        </fieldset>

		<fieldset class="link-rel">
			<legend>Relevanz</legend>
            <input type="text" id="relevance" name="relevance" readonly>
            <div id="slider"></div>
		</fieldset>

		<fieldset class="link-dest">
			<legend>Wo wird verlinkt?</legend>
            <div class="col1">
                <label for="homepage">Homepage:</label>
                <input type="checkbox" name="homepage" value="1" {if {$link.homepage}=="1" }checked{/if}>
            </div>
            <div class="col2">
                <label for="detail">Produkt:</label>
                <input type="checkbox" name="detail" value="1" {if {$link.detail}=="1" }checked{/if}>
            </div>
            <div class="col3">
                <label for="emotion">Einkaufswelt:</label>
                <input type="checkbox" name="emotion" value="1" {if {$link.emotion}=="1" }checked{/if}> </div>
            <div class="col1">
                <label for="category">Kategorie:</label>
                <input type="checkbox" name="category" value="1" {if {$link.category}=="1" }checked{/if}> </div>
            <div class="col2">
                <label for="article">Blogartikel:</label>
                <input type="checkbox" name="article" value="1" {if {$link.article}=="1" }checked{/if}> </div>
            <div class="col3">
                <label for="blog">Blockübersicht:</label>
                <input type="checkbox" name="blog" value="1" {if {$link.blog}=="1" }checked{/if}> </div>
		</fieldset>

        <div class="pull-right">
            <button type="button" id="abort" class="btn btn-success">Abbrechen</button>
            <button type="submit" id="go-save" class="btn btn-success">Speichern</button>
        </div>

    </div>
</form>
	

{/block}


{block name="content/layout/javascript"}
    <script type="text/javascript">
        
	$(function() {
		
		$( "#slider" ).slider({
			min: 1,
			max: 100,
			value: value="{$link.relevance}",
			slide: function( event, ui ) {
				$( "#relevance" ).val( ui.value );
			}
		});
    $( "#relevance" ).val( $( "#slider" ).slider( "value" ) );
		
			
		$('#abort').on('click' , function(event){
			postMessageApi.window.destroy();
		});	
			
			
		$('#form-link-detail').on('submit' , function(event){
			
			event.preventDefault();
			
			var values = {};
			$.each($('#form-link-detail').serializeArray(), function(i, field) {
			
				values[field.name] = field.value;
			});
			
			jQuery.ajax({
				url: "{url controller="InternalLinks" action="saveLink"}",
				type: "POST",
				dataType: "json",
				data: { data: values}, //set Data to send

				success: function (res) {
									
					postMessageApi.sendMessageToSubWindow({
                    component: 'main',
						params: {
							action: 'linksaved',
							param:  '{$eventlog}'
						}
					});
					
					postMessageApi.window.destroy();
				
				}
			});
			
		});
	
	});
		
    </script>
{/block}