{extends file="parent:backend/_base/layout.tpl"}

{block name="content/main"}{/block}

{block name="content/layout/javascript"}
    <script type="text/javascript">
        var subscriber = window.events.subscribe('get-post-message', function(eOpts) {
            var content = document.getElementsByClassName('js-content')[0];

            subscriber.remove();

            if(eOpts.result) {
                content.innerHTML = eOpts.result.msg;
            }
        });
    </script>
{/block}