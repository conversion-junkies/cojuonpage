{extends file="parent:backend/_base/layout.tpl"}

{block name="content/main"}


<!--

<h2>Daten Import & Export</h2>

<button id="export">Export File</button>
<div id="progress"></div>
<div id="progressBar"></div>
 
<input type="file" name="file">

-->
<div id="config-view" class="table-responsive">
	<div class="table-left">
		<div class="panel-display right">
			<span class="panel-trigger"></span>
			<span class="panel-text">Kompatibilitäts Manager</span>
		</div>	
		<div class="wrapper">
			<div class="top-description">
				<span>Kompatibilitäts Manager</span>	
			</div>	
			<div class="top-toolbar">
				<!--	<button id="load_default_keywords">Default Keywords</button> -->

				<span class="border-wrapper">
					<label>
						<select id="select_hook" name="select_hook">
							{foreach from=$typeNames key=key item=type}
								<option value="{$key}" rel="{$type.id}" >{$type.name}</option>
							{/foreach}
						</select>
					</label>
				</span>	

				<span class="border-wrapper">
					<label>
						<select id="select_subshop" name="select_subshop">
							{foreach from=$subshops item=shop}
								<option value="{$shop.id}" >{$shop.name}</option>
							{/foreach}
						</select>
					</label>
				</span>

				<span class="text">Anzahl: </span>
				<span class="border-wrapper">
					<input type="text" name="input_hook_value" id="input_hook_value" value="3" />
				</span>	

				<span class="text">Feld eintragen: </span>
				<span class="border-wrapper">
					<input type="text" id="name_field" name="name_field" value="" />
				</span>	
				
				<button id="add_hook" name="add_hook_button" type="submit" class="top-button addlink"><span class="icons plus-circle-frame"></span>Hinzufügen</button>
								
			</div>
			
			<table id="list_hook" class="table table-striped" cellspacing="0">
				<thead>
					<tr>
						<th>Integration in</th>
						<th>Shop</th>
						<th>Feld</th>
						<th>Anzahl</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{foreach from=$hooks item=hook}
					<tr rel="{$hook.id}"><td>{$hook.name}</td><td>{$subshops[{$hook.subshop}].name}</td><td>{$hook.value}</td><td> {$hook.count}</td><td><span class="coju_hook_delete"></span></td>
					{/foreach}
				</tbody>
			</table>

		</div>
	</div>

	<div class="table-right">
		<div class="panel-display left">
			<span class="panel-trigger"></span>
			<span class="panel-text">Ex & Import</span>
		</div>	
		<div class="wrapper">
			<div class="top-description">
				<span>Export & Import</span>	
			</div>	
			<div class="top-toolbar">
					<!--	<button id="load_default_keywords">Default Keywords</button> -->
			</div>


		</div>
	</div>
</div>
{/block}

{block name="content/javascript"}
    
	<script type="text/javascript" src="{link file="backend/_resources/js/FileSaver.min.js"}"></script>
	<script type="text/javascript" src="{link file="backend/_resources/js/simpleUpload.min.js"}"></script>
	
    <script type="text/javascript">
        
		var exkeywordtable = null;
		
		$(function() {
		
			$('#add_hook').on('click', function(){
								
				if( $('#name_field').val() ) {
									
					jQuery.ajax({
						url: "{url controller="InternalLinks" action="addHook"}",
						type: "POST",
						dataType: "json",
						data: { 'type': $('#select_hook').val() , 'subshop': $('#select_subshop').val() , 'field' : $('#name_field').val() , 'count' : $('#input_hook_value').val()  }, //set Data to send

						success: function (res) {
							
							$('#list_hook').append('<tr rel="' + res.data.id +'"><td>' + $('#select_hook option:selected').text() + '</td><td>' + $('#select_subshop option:selected').text() + '</td><td>' + res.data.value + '</td><td>' + res.data.count + '</td><td><span class="coju_hook_delete"></span></td>');
						
						}
					});
					
				}else {
					
					// @todo Message
				}
				
			});
			
			$('body').on('click' , '.coju_hook_delete' , function(){
				
				var that = this;
				
				jQuery.ajax({
					url: "{url controller="InternalLinks" action="deleteHook"}",
					type: "POST",
					dataType: "json",
					data: { id: $(this).parent().parent().attr('rel') }, //set Data to send

					success: function (res) {
					
						$(that).parent().parent().remove();
					
					}
				});
				
			});
			
		
			  $('input[type=file]').change(function(){
			 
					$(this).simpleUpload("{url controller="InternalLinks" action="setImportData"}", {
			 
						start: function(file){
							//upload started 
							$('#progress').html("");
							$('#progressBar').width(0);
						},
			 
						progress: function(progress){
							//received progress 
							$('#progress').html("Progress: " + Math.round(progress) + "%");
							$('#progressBar').width(progress + "%");
						},
			 
						success: function(data){
							//upload successful 
							$('#progress').html("Success!");
						},
			 
						error: function(error){
							//upload failed 
							$('#progress').html("Failure!<br>" + error.name + ": " + error.message);
						}
			 
					});
			 
				});
			
			
			$('#export').on('click' , function(){
		
				jQuery.ajax({
					url: "{url controller="InternalLinks" action="getExportData"}",
					type: "POST",
					dataType: "text",
					data: {}, //set Data to send

					success: function (res) {
						
						{literal}
						var file = new File(['"' + res + '"'] , "internallink.txt", {type: "Content-type: text"});
						saveAs(file);
						{/literal}
					
					}
				});
				
			});
			
			$('#load_default_keywords').on('click' , function(){
				
				jQuery.ajax({
					url: "{url controller="InternalLinks" action="getDefaultKeywords"}",
					type: "POST",
					dataType: "json",
					data: {}, //set Data to send

					success: function (res) {
					
						exkeywordtable.ajax.reload();
					
					}
				});
				
				
			});
		
			//var url = "{url controller="InternalLinks" action="getExclude"}",
            
		
			
			$('body').on('click' , '.coju_delete_stopword' , function(){
				
				jQuery.ajax({
					url: "{url controller="InternalLinks" action="deleteStopword"}",
					type: "POST",
					dataType: "json",
					data: { id: $(this).attr('rel') }, //set Data to send

					success: function (res) {
					
						exkeywordtable.ajax.reload();
					
					}
				});
				
		
			});
			
		});	
			
			
			
       
		
		
		
		
      
    
    </script>
{/block}