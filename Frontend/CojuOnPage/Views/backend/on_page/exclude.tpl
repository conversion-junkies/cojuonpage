{extends file="parent:backend/_base/layout.tpl"}

{block name="content/main"}
	<div class="wrapper">
		<div class="top-description">
			<span>Ausschließende Keywords</span>	
		</div>	
<div class="top-toolbar">
		<!--	<button id="load_default_keywords">Default Keywords</button> -->
</div>
        <table id="exkeywords" class="table table-striped" >
			<thead>
				<tr>
					<th>Term</th>
					<th>Language</th>
					<th>Action</th>
				</tr>
			</thead>
		</table>
		

		
</div>
{/block}

{block name="content/javascript"}
    
    <script type="text/javascript">
        
		var exkeywordtable = null;
		
		$(function() {
			
			exkeywordtable = jQuery('#exkeywords').DataTable( {
				"ajax": "{url controller="InternalLinks" action="getExclude"}",
				"order": [[ 0, "asc" ]],
				
				"pageLength": 20,
				"lengthMenu": [ 20, 50, 100 ],
				
				"columns": [
					{ "data": "term" },
					{ "data": "language" },
					{ "data": "" }		
				],
				{literal}
				"aoColumnDefs": [
						{"aTargets": [-1],
						"orderable": false,
						"mData": null,
						"mRender": function (data, type, full) {
		
							return '<i class="fa fa-fw fa-minus coju_delete_stopword" rel="' + full.id + '"></i>';
							
						}}
					]
				{/literal}
				
			}); 
		
			$('#load_default_keywords').on('click' , function(){
				
				jQuery.ajax({
					url: "{url controller="InternalLinks" action="getDefaultKeywords"}",
					type: "POST",
					dataType: "json",
					data: {}, //set Data to send

					success: function (res) {
					
						exkeywordtable.ajax.reload();
					
					}
				});
				
				
			});
		
			//var url = "{url controller="InternalLinks" action="getExclude"}",
            
		
			
			$('body').on('click' , '.coju_delete_stopword' , function(){
				
				jQuery.ajax({
					url: "{url controller="InternalLinks" action="deleteStopword"}",
					type: "POST",
					dataType: "json",
					data: { id: $(this).attr('rel') }, //set Data to send

					success: function (res) {
					
						exkeywordtable.ajax.reload();
					
					}
				});
				
		
			});
			
		});	
			
			
			
        {literal}
		
		
		
		
      
        {/literal}
    </script>
{/block}