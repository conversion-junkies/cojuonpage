{extends file="parent:backend/_base/layout.tpl"}

{block name="content/main"}
    
    <div class="table-responsive">
		<div class="table-left">
			
			{include file="backend/on_page/sidebar.tpl"}
			
		</div>
		<div class="table-right">
			
			<div class="panel-display right">
				<span class="panel-trigger"></span>
				<span class="panel-text">gefundene Keywords</span>
			</div>	
			<div class="wrapper">
				<div class="top-description">
					<span>Links</span>	
				</div>	
			
				<div class="top-toolbar">
					<button type="submit" class="top-button addlink"><span class="icons plus-circle-frame"></span>Hinzufügen</button>
					<button type="submit" class="top-button removelink"><span class="icons minus-circle-frame"></span>Markierte Einträge löschen</button>
				</div>
			
				<table id="links" class="table table-striped" cellspacing="0" >
					 <thead>
					<tr>
						<th><input type="checkbox" class="checkbox-select-all" value="" /></th>
						<th>Term</th>
						<th>Link</th>
						<th>Title</th>
						<th title="Relevance">R</th>
						<th title="Follow/NoFollow">F</th>
						<th title="Startseite">H</th>
						<th title="Produktseite">P</th>
						<th title="Einkaufswelt">E</th>
						<th title="Kategorie">K</th>
						<th title="Blogartikel">A</th>
						<th title="Blogübersicht">B</th>
						<th>Action</th>
					</tr> 
					</thead>
			 
				</table>
			</div>
		
		</div>
    </div>
	
{/block}

{block name="content/javascript"}
   
	
	<script type="text/javascript">
		
	   
	var linktable = null;	
	var keywordtable = null;	
			
		
	var subscriber = window.events.subscribe('get-post-message', function(eOpts) {
			
			if(eOpts.result.action == 'linksaved' ) {
								
				switch(eOpts.result.param) {
					case "keyword":
						keywordtable.ajax.reload();
						linktable.ajax.reload();
						break;
					case "new":
					case "edit":
					default:
						linktable.ajax.reload();
						
				} 
			}
        });
		
		//@todo Specific for a Table
		$('.addlink').on('click', function(event) {
			
			var $this = $(this);
            event.preventDefault();
			
			var values = {
                width: 580,
                height: 580,
                component: 'customSubWindow',
				url: "{url controller="InternalLinks" action="linkDetail" subshop="{$subshop}"}",
                title: 'Add Link'
            };
			
            postMessageApi.createSubWindow(values);
			
        });
		
		$('.removelink').on('click', function(event) {
			
			var $this = $(this);
            event.preventDefault();
			
			var checkboxes = $('#links').find('.checkbox-select:checked');
			var array = new Array();
			
            $.each( checkboxes, function( index, value ) {
			   array.push($(this).attr('rel'));
			});	
			
			jQuery.ajax({
				url: "{url controller="InternalLinks" action="deleteLinks"}",
				type: "POST",
				dataType: "json",
				data: { ids: array }, //set Data to send

				success: function (res) {
					linktable.ajax.reload();
				}
			});
			
        });
		
		function checkTrueIcon(value){
			
			if(value == 1){
				return '<i class="fa fa-fw fa-check"></i>';
			}else {
				return '<i class="fa fa-fw fa-close"></i>';
			
			}
		
		}
		
		
	$(function() {

		// MISC FUNCTIONS 
		
		jQuery('.checkbox-select-all').on('change' , function(){
			var 
			check = $(this).context.checked ,
			checkboxes = jQuery(this).closest('.table').find('.checkbox-select');
			$.each( checkboxes, function( index, value ) {
			   $(this).prop('checked', check);
			});			
		});
		
		
		linktable = jQuery('#links').DataTable( {
			oLanguage: {
				sProcessing: "<div class=\"spinner\">Loading Keywordlist</div>"
			},
			"order": [[ 1, "asc" ]],
			"pageLength": 20,
			"lengthMenu": [ 20, 50, 100 ],
			"ajax": '{url controller="InternalLinks" action="getLinks" subshop="{$subshop}"}',
			"dom": 'ftplri',
			"columns": [
				{ "data": "" },
				{ "data": "term" },
				{ "data": "link" },
				{ "data": "title" },
				{ "data": "relevance" },
				{ "data": "follow" },
				{ "data": "homepage" },
				{ "data": "detail" },
				{ "data": "emotion" },
				{ "data": "category" },
				{ "data": "article" },
				{ "data": "blog" },
				{ "data": "" }
				
			],
			{literal}
			"aoColumnDefs": [
					{"aTargets": [0],
					"mData": null,
					"orderable": false,
					"mRender": function (data, type, full) {
						return '<input type="checkbox" class="checkbox-select" name="checkbox-select" rel="' + full.id + '" value="" />';
					}},
					{"aTargets": [2],
					"mData": null,
					"mRender": function (data, type, full) {
						return '<span title="' + full.link + '">' + full.link.substr(0, 50) + '</span>';
					}},
					{"aTargets": [-1],
					"orderable": false,
					"mData": null,
					"mRender": function (data, type, full) {
						return '<i class="fa fa-fw fa-pencil coju_open_link" rel="' + full.id + '"></i>';
					}},
					{"aTargets": [-2,-3,-4,-5,-6,-7,-8,],
					"orderable": false,
					"mData": null,
					"mRender": function (data, type, full) {						
						return checkTrueIcon(data);
					}}
				]
			{/literal}
		}); 
		
	});
		

		keywordtable = jQuery('#keywords').DataTable( {
			"ajax": '{url controller="InternalLinks" action="getkeywords" subshop="{$subshop}"}',
			"order": [[ 2, "DESC" ]],
			"pageLength": 20,
			"lengthMenu": [ 20, 50, 100 ],
			"columns": [
				{ "data": "" },
				{ "data": "term" },
				{ "data": "count" },
				{ "data": "" },
				{ "data": "" },
				
			],
			{literal}
			"aoColumnDefs": [
					{"aTargets": [0],
					"mData": null,
					"orderable": false,
					"mRender": function (data, type, full) {
						return '<input type="checkbox" class="checkbox-select" name="checkbox-select" value="" />';
					}},
					{"aTargets": [-2],
					"orderable": false,
					"mData": null,
					"mRender": function (data, type, full) {
	
						return '<i class="coju_load_metrics" rel="' + full.term + '">metrik laden</i>  ';
						
					}},
					{"aTargets": [-1],
					"orderable": false,
					"mData": null,
					"mRender": function (data, type, full) {
	
						return '<i class="fa fa-fw fa-plus coju_add_to_link" rel="' + full.term + '"></i>    <i class="fa fa-fw fa-eraser coju_exclude_keyword" rel="' + full.term + '"></i>';
						
					}}
				]
			{/literal}
			
		}); 
		
		$('body').on('click' , 'i.coju_load_metrics' , function(){
			//console.log('EXCLUDE');
			
			var that = this;

			jQuery.ajax({
				url: "{url controller="InternalLinks" action="loadMetrics"}",
				type: "POST",
				dataType: "json",
				data: { term: $(this).attr('rel') }, //set Data to send

				success: function (res) {				
					$(that).html('<span title="Suchvolumen">' + res.data.searchvolume + '</span> | <span title="CPC">' + res.data.cpc + '&euro;</span>');
				}
			});
			
		});
		
		$('body').on('click' , 'i.coju_exclude_keyword' , function(){
			//console.log('EXCLUDE');
			
			var that = $(this).parentsUntil('tr').parent();
			
			jQuery.ajax({
				url: "{url controller="InternalLinks" action="addStopword"}",
				type: "POST",
				dataType: "json",
				data: { term: $(this).attr('rel'), language : "de_DE" }, //set Data to send

				success: function (res) {
					keywordtable.ajax.reload();
				}
			});
		});
		
		$('body').on('click' , 'i.coju_add_to_link' , function(event){
		
            var $this = $(this);
            event.preventDefault();
			
			var values = {
                width: 580,
                height: 580,
                component: 'customSubWindow',
                url: 'InternalLinks/linkDetail/subshop/{$subshop}/term/' + $(this).attr('rel') ,
                title: 'Add Link'
            };
			
            postMessageApi.createSubWindow(values);
        });
		
		
		$('body').on('click' , 'i.coju_open_link' , function(event){
		
            var $this = $(this);
			
            event.preventDefault();
			
			var values = {
                width: 580,
                height: 580,
                component: 'customSubWindow',
                url: 'InternalLinks/linkDetail/subshop/{$subshop}/id/' + $(this).attr('rel') ,
                title: 'Add Link'
            };
			
            postMessageApi.createSubWindow(values);
        });
		
		
    </script>
{/block}