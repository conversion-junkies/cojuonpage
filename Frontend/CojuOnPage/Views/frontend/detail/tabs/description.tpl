{extends file="parent:frontend/detail/tabs/description.tpl"}				
				
{block name='frontend_detail_actions_contact'}
	{if $sInquiry}
		<li class="list--entry">
			{s name="DetailLinkContact" namespace="frontend/detail/actions" assign="snippetDetailLinkContact"}{/s}
			<a href="{$sInquiry}" class="content--link link--contact" title="{$snippetDetailLinkContact|escape}">
				<i class="icon--arrow-right"></i> {s name="DetailLinkContact" namespace="frontend/detail/actions"}{/s}
			</a>
		</li>
	{/if}
{/block}