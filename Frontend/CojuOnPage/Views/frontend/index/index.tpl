{extends file="parent:frontend/index/index.tpl"}

{block name='frontend_index_sidebar_after_filter'}
{if $sCategoryContent.attribute.coju_onpage_sidebar}

	{$sCategoryContent.attribute.coju_onpage_sidebar}

{/if}
{/block}

{block name='frontend_index_footer_copyright'}


<div class="container">

{if $sFooterSuppliers}
 
<span class="column--headline topsupplier">Auszug unserer Designer / Hersteller</span>
<ul class="topsuppliers">	
  {foreach item=supplier from=$sFooterSuppliers}
	<li>
		<a href="{url controller='listing' action='manufacturer' sSupplier=$supplier.id}" class="footer--supplier-link" title="{$supplier.name}">{$supplier.name}</a>
	</li>
  {/foreach}
</ul>  
{/if}


{if $sFooterCategories}
<br />
<br />
<br />
<span class="column--headline topcategory">Kategorienübersicht</span>
<ul class="topcategories">	
  {foreach item=category from=$sFooterCategories}
	<li class="navigation--entry" role="menuitem">                                   
		<a class="navigation--link" href="{$category.link}" title="{$category.description}" itemprop="url">
			<span itemprop="name">{$category.description}</span>
		</a>
	</li>
  {/foreach}
</ul>  
{/if}

</div>







{/block}



