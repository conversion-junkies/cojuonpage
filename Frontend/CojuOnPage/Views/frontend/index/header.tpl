{extends file="parent:frontend/index/header.tpl"} 


{block name='frontend_index_header_meta_robots'}{if $coju_onpage_deindex}noindex,follow{else}{$smarty.block.parent}{/if}{/block}



{block name="frontend_index_header_javascript_tracking" append}

	{if $coju_onpage_schema}

    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "address": {
        "@type": "PostalAddress",
        {if $coju_onpage_schema.city}"addressLocality": "{$coju_onpage_schema.city}",{/if}
        {if $coju_onpage_schema.postcode}"postalCode": "{$coju_onpage_schema.postcode}",{/if}
        {if $coju_onpage_schema.address}"streetAddress": "{$coju_onpage_schema.address}"{/if}
      },
      "name": "{$coju_onpage_schema.name}"
    }
    </script>

	{/if}



{/block}