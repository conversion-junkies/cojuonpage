{extends file="parent:frontend/listing/sidebar.tpl"} 



{block name="frontend_index_sidebar"}
    {if $theme.sidebarFilter}
        {block name='frontend_listing_sidebar'}
            <div class="listing--sidebar">
                {$smarty.block.parent}
			
				{if $sCategoryContent.attribute.coju_onpage_sidebar}

					{$sCategoryContent.attribute.coju_onpage_sidebar}

				{/if}
				
            </div>
        {/block}
    {else}
        
    {/if}
{/block}
