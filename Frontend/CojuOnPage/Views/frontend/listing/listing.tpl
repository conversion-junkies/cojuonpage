{extends file="parent:frontend/listing/listing.tpl"}

{* SEO FOOTER *}
{block name='frontend_listing_listing_wrapper' append}
	
	{if $sCategoryContent.attribute.coju_onpage_extended}
        <div class="footer--column is--seotext-footer is--last block">
            <div id="seoExtendedText" class="column--content">
                {$sCategoryContent.attribute.coju_onpage_extended}	
            </div>
        </div>
    {/if}
{/block}


{block name="frontend_listing_listing_content"}
	<div class="listing"
		 data-ajax-wishlist="true"
		 data-compare-ajax="true"
			
			{*if $theme.infiniteScrolling}
				data-infinite-scrolling="true"
				data-loadPreviousSnippet="{s name="ListingActionsLoadPrevious"}{/s}"
				data-loadMoreSnippet="{s name="ListingActionsLoadMore"}{/s}"
				data-categoryId="{$sCategoryContent.id}"
				data-pages="{$pages}"
				data-threshold="{$theme.infiniteThreshold}"
			{/if*}>

		{* Actual listing *}
		{block name="frontend_listing_list_inline"}
			{foreach $sArticles as $sArticle}
				{include file="frontend/listing/box_article.tpl"}
			{/foreach}
		{/block}
	</div>
{/block}